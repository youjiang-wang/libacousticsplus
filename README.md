# libAcousticsPlus

An OpenFOAM library to calculate flow induced acoustic signals using Lighthill / Curle / Ffowcs Williams-Hawkings acoustic analogies.

## Attribution

The authors thank the financial support by the EU Interreg project Green Cruising for Small High Speed Craft (Grant No.: 605111) and Startup Funding of Shanghai Jiao Tong University (Grant No.: WH220401011) during the development of the primary version of libAcousticsPlus.

When using this library in your research, please cite following article(s):

[1] Youjiang Wang, Tommi Mikkola, Spyros Hirdaris,
A fast and storage-saving method for direct volumetric integration of FWH acoustic analogy,
Ocean Engineering,
Volume 261,
2022,
112087,
ISSN 0029-8018,
https://doi.org/10.1016/j.oceaneng.2022.112087.

## Usage

The libAcousticsPlus is to be used as a functionObject with OpenFOAM. Thus, one needs to install OpenFOAM correclty before using libAcousticsPlus.

The way to turn on libAcousticsPlus and use its functionalities is given in the simple tutorials. The acoustic calculations can be executed on the fly or as a postprocess procedure. The acoustic results are saved in *caseDir/postProcessing/functionName*, these includes temporal pressure oscillation, octave sound pressure level, one third octave sound pressure level, etc. If saving sound source is turned on, the surface source and volume source are saved in sub-directories *surfaceSourceFWH* and *volumeSource*, respectively, in an OpenFOAM case format. This facilitates the visualization of sound source using ParaView or other tools. It also makes the reloading of info into OpenFOAM in a post-processing calculation easily.

To use the dual mesh technique, one needs to generate the acoustic mesh before calculation. The acoustic mesh is located as another mesh region,
and its name is given in the function dictionary (default is acousticMesh).
A uniform Cartesian mesh is usually enough. This can be simply done in place using *blockMesh* utility with several terminal parameters. One needs to add a simple *regionProperties* file in constant directory to enable correct mesh decomposition. Please refer to the tutorial *cavitatingBulletParallel* for more information.

The recommend order to go through the tutorials is cavitatingBullet -> cavitatingBulletParallel -> propellerWet.

Although not fully tested, libAcousticsPlus has functionalities as following
1. Switch between different acoustic analogies by turning off/on volumetric sources.
2. Execute acoustic evalutations on the fly or as a post-processing.
3. Define different sound source surfaces during CFD simulation / in the post-processing.
4. Use CFD mesh or acoustic mesh for volumetric sound source (dual mesh technique).
5. Restrict volumetric sound source to specific CFD mesh zones or/and acoustic mesh zones.
6. Define interval for acoustic evaluations and saving sound source.
7. Consider Moving / convective observers.
8. Setting observers with mesh surface.
9. Setting permeable surface as acoustic source surface.
10. Consider multiphase flow including cavitation.
11. Conduct FFT evaluation, calculate acoustic power spectrum.

## The name

The name libAcousticsPlus is to distinct itself from the existing library libacoustics from unicfdlab. Plus also means we have added and plan to add additional functionalities other than traditional acoustic analogies. These includes until now: 1) The fast and storage-saving dual mesh technique.

## Support
Contact Prof. Youjiang Wang (youjiang.wang@sjtu.edu.cn) for scientific discussions.

## License
The same as OpenFOAM.
