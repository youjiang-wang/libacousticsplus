/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2017-2020 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "acousticAnalogyIntegral.H"
#include "IOmanip.H"

namespace Foam
{
namespace functionObjects
{
    defineTypeNameAndDebug(acousticAnalogyIntegral, 0);
}
}

// * * * * * * * * * * * * * private memeber functions  * * * * * * * * * * //
void Foam::functionObjects::acousticAnalogyIntegral::initializeCavVolFile()
{
    if (Pstream::master() && foundObject<volScalarField>("alpha.vapour"))
    {
        fileName fName = aaRootPath()/"cavVol";
        if (checkOldFile(fName))
        {
            cavVolFilePtr_.reset(new OFstream(fName));
        }
        else
        {
            FatalErrorInFunction
                << "Fail to save old file for " << fName << abort(FatalError);
        }
        cavVolFilePtr_->setf(ios_base::scientific, ios_base::floatfield);
        cavVolFilePtr_->precision(8);
        cavVolFilePtr_->width(16);
        cavVolFilePtr_() << setw(2) << "# "
                         << setw(14) << "Cavitation Volume" << nl;
        cavVolFilePtr_() << setw(2) << "# " << setw(14)
                         << "Time" << setw(16) << "cavVolume"
                         << setw(16) << "ZoneCavVol" << endl;
    }
    // Info << "Obr Classes:" << nl << obr().classes() << endl;
    // Info << "Obr Names:" << nl << obr().names() << endl;
}

void Foam::functionObjects::acousticAnalogyIntegral::calculateCavVol()
{
    if (foundObject<volScalarField>("alpha.vapour"))
    {
        const volScalarField& alpha = lookupObject<volScalarField>("alpha.vapour");
        scalar cavVol = gSum(alpha.primitiveField() * mesh_.V().field());

        scalar partCavVol = 0.;
        if(!cfdZoneIds().empty())
        {
            bitSet cfdCells(mesh_.nCells());
            for (label zonei: cfdZoneIds())
            {
                cfdCells.set(mesh_.cellZones()[zonei]);
            }

            for (label celli = 0; celli < mesh_.nCells(); ++celli)
            {
                if (cfdCells[celli])
                    partCavVol += alpha[celli] * mesh_.V()[celli];
            }
            reduce(partCavVol, sumOp<scalar>());
        }
        else
        {
            partCavVol = cavVol;
        }

        if (Pstream::master())
        {
            cavVolFilePtr_()
                << setw(16) << time_.value()
                << setw(16) << cavVol
                << setw(16) << partCavVol << endl;
        }
    }
}

// * * * * * * * * * * * * * protected memeber functions  * * * * * * * * * * //
bool Foam::functionObjects::acousticAnalogyIntegral::checkOldFile
(
    const fileName& fName
) const
{
    bool ok = true;
    if (isFile(fName))
    {
        fileName oldName = fName + "_old";
        label oldVersion = 0;
        while (isFile(oldName + Foam::name(oldVersion)))
        {
            ++oldVersion;
        }
        oldName += Foam::name(oldVersion);
        ok = fileHandler().mv(fName, oldName);
    }
    return ok;
}

//- calculate sound pressure
void Foam::functionObjects::acousticAnalogyIntegral::calcSoundPressure()
{
    if (debug)
    {
        Info << incrIndent;
        Info << "Calculating sound pressure" << nl;
    }

    observerSet_.extendMemory();
    if (volumeSource())
    {
        calcVolSoundPressure();
    }
    calcSrfSoundPressure();

    if (debug)
    {
        Info << decrIndent;
    }
}

void Foam::functionObjects::acousticAnalogyIntegral::writeSoundPressure()
{
    // write the finished one & truncate the sound pressure list if necessary
    // TODO
    observerSet_.flushSoundPressure();
}

Foam::instantList
Foam::functionObjects::acousticAnalogyIntegral::sourceTimes() const
{
    instantList ssTimes = surfaceSourceTimes();
    if (!volumeSource())
    {
        return ssTimes;
    }

    instantList vsTimes = volumeSourceTimes();

    if (vsTimes.empty())
    {
        if (debug)
        {
            Info << indent << "No volumece source, take only the surface source." << endl;
        }

        return ssTimes;
    }
    if (ssTimes.empty())
    {
        if (debug)
        {
            Info << indent << "No surface source, take only the volume source." << endl;
        }
        return vsTimes;
    }
    if (vsTimes != ssTimes)
    {
        FatalErrorInFunction
            << "volume source and surface source do not have the same times."
            << abort(FatalError);
    }

    return vsTimes;
}

void Foam::functionObjects::acousticAnalogyIntegral::movingRVector
(
    vectorField& rVector
) const
{
    if (!U0NonZero())
    {
        return;
    }
    const vector eU = U0_ / magU0_;
    const scalar M = magU0_ / c0_;
    const scalar MSqr = M * M;
    const scalar betaSqr = 1.0 - MSqr;

    scalarField rU0 = rVector & eU;
    rVector +=
    (
        (
            M * rU0
          + Foam::sqrt(betaSqr * Foam::magSqr(rVector) + MSqr * Foam::sqr(rU0))
        ) * (M / betaSqr)
    ) * eU;

    return;
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::functionObjects::acousticAnalogyIntegral::acousticAnalogyIntegral
(
    const word& name,
    const Time& runTime,
    const dictionary& dict
)
:
    acousticAnalogy(name, runTime, dict),
    c0_(1500),
    U0_(Zero),
    magU0_(0.),
    spFFT_(false),
    calcSoundSource_(true),
	calcSoundSourceInPost_(false),
    calcSoundPressure_(true),
    farfieldSimplification_(true),
    observerSet_(*this, runTime, dict.subDict("observers")),
    cavVolFilePtr_()
{
    read(dict);
    observerSet_.initilize();

    if (debug && !postProcess)
    {
        initializeCavVolFile();
    }
}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //
bool Foam::functionObjects::acousticAnalogyIntegral::read(const dictionary& dict)
{
    if(!acousticAnalogy::read(dict))
    {
        return false;
    }

    dict.readIfPresent<scalar>("c0", c0_);
    Info << "    Sound speed c0 = " << c0_ << endl;

    if (dict.found("U0"))
    {
        U0_ = dict.get<vector>("U0");
        magU0_ = Foam::mag(U0_);
    }
    Info << "    Observer speed U0 = " << U0_ << ", magU0 = " << magU0_ << endl;

    if (postProcess)
    {
        dict.readIfPresent("calculateSoundSourceInPost", calcSoundSourceInPost_);
        if (calcSoundSourceInPost_)
        {
            Info << "    Do sound source calculation (Post)." << endl;
        }
        else
        {
            Info << "    No sound source calculation (Post)." << endl;
        }
    }
    else
    {
		dict.readIfPresent("calculateSoundSource", calcSoundSource_);
		if (calcSoundSource_)
		{
			Info << "    Do sound source calculation." << endl;
		}
		else
		{
			Info << "    No sound source calculation." << endl;
		}
    }


    dict.readIfPresent("calculateSoundPressure", calcSoundPressure_);
    if (calcSoundPressure_)
    {
        Info << "    Do sound pressure calculation." << endl;
    }
    else
    {
        Info << "    No sound pressure calculation." << endl;
    }

    dict.readIfPresent("farfieldSimplification", farfieldSimplification_);
    if (farfieldSimplification_)
    {
        Info << "    Do farfield simplification." << endl;
    }
    else
    {
        Info << "    No farfield simplication." << endl;
    }

    Info << endl;
    return true;
}

bool Foam::functionObjects::acousticAnalogyIntegral::execute()
{
    if(!postProcess)
    {
        if (calcSoundPressure_)
        {
            calcSource();
            calcSoundPressure();
        }
        if (debug)
        {
            calculateCavVol();
        }
    }
    return true;
}

bool Foam::functionObjects::acousticAnalogyIntegral::write()
{
    if(!postProcess)
    {
        if (calcSoundSource_ && !calcSoundPressure_)
        {
            calcSource();
        }
        if (calcSoundSource_){
            writeSource();
        }
        if (calcSoundPressure_)
        {
            // calculate has already been done in execute()
            // here only write is necessary
            writeSoundPressure();
        }
    }
    else // in postProcess
    {
        if (calcSoundSourceInPost_)
        {
            calcSource();
            writeSource();
        }
    }
    return true;
}

bool Foam::functionObjects::acousticAnalogyIntegral::end()
{
	time_.printExecutionTime(Info);
    if (postProcess)
    {
        if (calcSoundPressure_)
        {
            if (debug)
            {
                Info << "Calculating sound pressure in end() " << endl;
            }

            instantList timeDirs = sourceTimes();
            const_cast<fvMesh&>(acousticMesh()).objectRegistry::rename(polyMesh::defaultRegion);

            if (debug)
            {
                Info << incrIndent;
            }
            forAll(timeDirs, timei)
            {
                if(timeDirs[timei].name() == "constant")
                {
                    continue;
                }
                if (debug)
                {
                    Info << indent << "Time = " << timeDirs[timei].name() << endl;
                }

                Time& time = const_cast<Time&>(time_);
                time.setTime(timeDirs[timei], timei);
                loadSource(timeDirs[timei]);
                calcSoundPressure();
                writeSoundPressure();
            }

            if (debug)
            {
                Info << decrIndent;
            }
        }
        time_.printExecutionTime(Info);
    }
    if (calcSoundPressure_)
    {
        observerSet_.flushSoundPressure(1);
    }
    if (observerSet_.FourierTransform())
    {
        observerSet_.calcSPL();
    }
    time_.printExecutionTime(Info);
    return true;
}
