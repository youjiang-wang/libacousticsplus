/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2017-2020 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::functionObjects::observerSet

Group
    grpFieldFunctionObjects

Description
    description

Note
    put notes here

Usage
    put usage here

See also
    - Foam::functionObject
    - Foam::functionObjects::fvMeshFunctionObject

SourceFiles
    observerSet.C

\*---------------------------------------------------------------------------*/


#ifndef functionObjects_observerSet_H
#define functionObjects_observerSet_H

#include "Enum.H"
#include "point.H"
#include "OFstream.H"
#include "MeshedSurface.H"
#include "surfaceWriter.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace functionObjects
{

class acousticAnalogyIntegral;
/*---------------------------------------------------------------------------*\
                            Class observerSet Declaration
\*---------------------------------------------------------------------------*/

class observerSet
{
    enum class inputType
    {
        POINT,
        SURFACE
    };
    
    static const Enum<inputType> inputTypeNames_;
    
    // Private Data
        const acousticAnalogyIntegral& aaIntegral_;
        
        const Time& time_;
    
        scalar startTime_, endTime_, timeStep_, memStartTime_;
        
        List<scalar> t_, freq_;
        
        Switch autoOffset_;
        
        List<scalar> timeOffsets_;
        
        scalar delayMin_;
        
        scalar delayMax_;
                
        scalarListList soundPressure_;
        
        List<point> observerPositions_;
        
        //- The file to write result of pDash, one file for each point
        autoPtr<OFstream> rawFilePtr_;
        
        label fieldWidth_;
        
        MeshedSurface<face> inputSurface_;
        
        //- format is according to "writeFormat"
        autoPtr<surfaceWriter> surfaceWriterPtr_;
        
        Switch FourierTransform_;
        
        scalar pDashRef_;
        
        scalar endFreq_;
        
    // Private Member Functions
        void writeSoundPressureHeader();
        
        bool loadSoundPressure
        (
            const fileName& fName,
            scalarList& timeList,
            scalarListList& soundPressure
        ) const;
        
        tmp<Field<scalarField>> octaveBands
        (
            const scalarField& freqList, 
            const Field<scalarField>& amplSqr, 
            scalarField& bandsCentre
        ) const;
        
        tmp<Field<scalarField>> oneThirdOctaveBands
        (
            const scalarField& freqList, 
            const Field<scalarField>& amplSqr, 
            scalarField& bandsCentre
        ) const;
        
        tmp<Field<scalarField>> calcBands
        (
            const scalar factor,
            const scalarField& freqList, 
            const Field<scalarField>& amplSqr, 
            scalarField& bandsCentre
        ) const;
        
        void writeFreqResult
        (
            const fileName& fName,
            const scalarField& freqList,
            const Field<scalarField>& result
        ) const;
        
        void calcTimeOffsets();
public:

    //- Runtime type information
    TypeName("observerSet");

    // Constructors

        //- Construct from Time and dictionary
        observerSet
        (
            const acousticAnalogyIntegral& parent,
            const Time& runTime,
            const dictionary& dict
        );

        //- No copy construct
        observerSet(const observerSet&) = delete;

        //- No copy assignment
        void operator=(const observerSet&) = delete;


    //- Destructor
    virtual ~observerSet() = default;
    
    // Member functions
        bool read(const dictionary& dict);
        
        void initilize();
        
        void setStartTime(const scalar& startTime);
        
        void extendMemory();
        
        void flushSoundPressure(const label minFlushSize=10);
        
        //- Do FFT and transform result to user-defined bands (three bands automatically ?)
        //- Results ared written in different files
        void calcSPL();
        
        //- 2 x necessary Time without cache
        void addSoundPressure
        (
            const label& observerId, 
            const scalar& oldTime, 
            const scalar& newTime,
            const scalar& oldSP,
            const scalar& newSP
        );
    
    // Inline member functions
        const List<point>& observerPositions() const;
        
        const scalar& startTime() const;
        
        const Switch& autoOffset() const;
        
        const Switch& FourierTransform() const;
        
        const scalar& delayMax() const;
        
        const scalar& delayMin() const;
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace functionObjects
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

inline const Foam::List<Foam::point>&
Foam::functionObjects::observerSet::observerPositions() const
{
    return observerPositions_;
}

inline const Foam::scalar&
Foam::functionObjects::observerSet::startTime() const
{
    return startTime_;
}

inline const Foam::Switch&
Foam::functionObjects::observerSet::autoOffset() const
{
    return autoOffset_;
}

inline const Foam::Switch&
Foam::functionObjects::observerSet::FourierTransform() const
{
    return FourierTransform_;
}

inline const Foam::scalar&
Foam::functionObjects::observerSet::delayMin() const
{
    return delayMin_;
}

inline const Foam::scalar&
Foam::functionObjects::observerSet::delayMax() const
{
    return delayMax_;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
