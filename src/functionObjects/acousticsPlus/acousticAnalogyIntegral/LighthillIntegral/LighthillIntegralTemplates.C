/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2017-2020 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "LighthillIntegral.H"
#include "fvcDiv.H"

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template<class Type>
void Foam::functionObjects::LighthillIntegral::interpolate(
    const Field<Type>& srcField, 
    Field<Type>& tgtField
)
{
    tgtField = Zero;
    if (hasAcousticMesh())
    {
        if(!cfdZoneIds().empty())
        {
            List<Type> srcSub(IndirectList<Type>(srcField, cfdCellMap_));
            mmPtr_().mapSrcToTgt(srcSub, plusEqOp<Type>(), tgtField);
        }
        else
        {
            mmPtr_().mapSrcToTgt(srcField, plusEqOp<Type>(), tgtField);
        }
        tgtField /= acousticMesh().V().field();
    }
    else
    {
        if(!cfdZoneIds().empty())
        {
            for (label celli : cfdCellMap_)
            {
                tgtField[celli] = srcField[celli];
            }
        }
        else
        {
            tgtField = srcField;
        }        
    }
    
    if (debug && hasAcousticMesh())
    {
        Type srcSum = Zero;
        if (!cfdZoneIds().empty())
        {
            List<Type> srcSub(IndirectList<Type>(srcField, cfdCellMap_));
            forAll (srcSub, i)
            {
                srcSum += srcSub[i] * mesh_.V()[cfdCellMap_[i]];
            }
        }
        else
        {
            forAll (srcField, i)
            {
                srcSum += srcField[i] * mesh_.V()[i];
            }
        }
    
        Type tgtSum = Zero;
        forAll (tgtField, i)
        {
            tgtSum += tgtField[i] * acousticMesh().V()[i];
        }
    
        reduce(srcSum, sumOp<Type>());
        reduce(tgtSum, sumOp<Type>());
    
        Info << indent << "source sum : " << srcSum << "  target sum : " << tgtSum << nl; 
    }
}

template<class Type>
void Foam::functionObjects::LighthillIntegral::checkVariable(Field<Type>& psi)
{
    const scalar nCells = acousticMesh().nCells();
    if (psi.size() != nCells)
    {
        psi.resize(nCells, Zero);
    }
}


template<class Type>
void Foam::functionObjects::LighthillIntegral::acousticDdt
(
    const Field<Type>& psi0, 
    const Field<Type>& psi, 
    Field<Type>& dt, 
    const scalar& rDeltaT
)
{
    dt = (psi - psi0) * rDeltaT;
}

// ************************************************************************* //