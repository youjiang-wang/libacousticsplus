/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2017-2020 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "LighthillIntegral.H"
#include "fvMeshSubset.H"
#include "scalarIndList.H"
#include "turbulentTransportModel.H"
#include "turbulentFluidThermoModel.H"
#include "nearestFaceAMI.H"

namespace Foam
{
namespace functionObjects
{
    defineTypeNameAndDebug(LighthillIntegral, 0);
    addToRunTimeSelectionTable
    (
        functionObject, LighthillIntegral, dictionary
    );

    fileName LighthillIntegral::vsDir = "volumeSource";
}
}

using Foam::constant::mathematical::pi;

#define vectorToSymmTensor(vectorField, tensorField)                      \
tensorField.replace                                                       \
(                                                                         \
    symmTensor::XX,                                                       \
    sqr(vectorField.component(vector::X))                                 \
);                                                                        \
tensorField.replace                                                       \
(                                                                         \
    symmTensor::XY,                                                       \
    vectorField.component(vector::X) * vectorField.component(vector::Y)   \
);                                                                        \
tensorField.replace                                                       \
(                                                                         \
    symmTensor::XZ,                                                       \
    vectorField.component(vector::X) * vectorField.component(vector::Z)   \
);                                                                        \
tensorField.replace                                                       \
(                                                                         \
    symmTensor::YY,                                                       \
    sqr(vectorField.component(vector::Y))                                 \
);                                                                        \
tensorField.replace                                                       \
(                                                                         \
    symmTensor::YZ,                                                       \
    vectorField.component(vector::Y) * vectorField.component(vector::Z)   \
);                                                                        \
tensorField.replace                                                       \
(                                                                         \
    symmTensor::ZZ,                                                       \
    sqr(vectorField.component(vector::Z))                                 \
);


// * * * * * * * * * * * * Private Member Functions * * * * * * * * * * * //


bool checkNAN(const Foam::scalarField& field, const Foam::word& name, bool& hasnanT)
{
    bool hasnan = false;
    Foam::DynamicList<Foam::label> ids(1000);
    for (int i = 0; i < field.size(); ++i)
    {
        double val = field[i];
        if (std::isnan(val))
        {
            hasnan = true;
            ids.append(i);
        }
    }
    if (hasnan)
    {
        Foam::Pout<< "Has NAN in " << name << ": " << ids << Foam::endl;
        hasnanT = true;
    }
    return hasnan;
}

bool checkNAN(const Foam::symmTensorField& field, const Foam::word& name, bool& hasnanT)
{
    bool hasnan = false;
    Foam::DynamicList<Foam::label> ids(1000);
    for (int fi = 0; fi < field.size(); ++fi)
    {
        Foam::symmTensor tensor = field[fi];
        for (int ci = 0; ci < tensor.size(); ++ci)
        {
            double val = tensor[ci];
            if (std::isnan(val))
            {
                hasnan = true;
                ids.append(fi);
            }
        }
    }
    if (hasnan)
    {
        Foam::Pout<< "Has NAN in " << name << ": " << ids << Foam::endl;
        hasnanT = true;
    }
    return hasnan;
}

void Foam::functionObjects::LighthillIntegral::checkInterpolationMap()
{
    const fvMesh& aMesh = acousticMesh();
    if (hasAcousticMesh() && !mmPtr_)
    {
        fvMeshSubset cfdMeshSubset(mesh_, cfdCellMap_);

        if(!cfdZoneIds().empty())
        {
            mmPtr_.reset
            (
                new meshToMesh
                (
                    cfdMeshSubset.subMesh(),
                    aMesh,
                    // "cellVolumeWeight",
                    "semiDirectMethod",
                    nearestFaceAMI::typeName,
                    HashTable<word>(0),     // patchMap,
                    wordList(0),            // cuttingPatches
                    meshToMesh::procMapMethod::pmAABB,
                    false                   // do not normalise
                )
            );
        }
        else
        {
            mmPtr_.reset
            (
                new meshToMesh
                (
                    mesh_,
                    aMesh,
                    // "cellVolumeWeight",
                    "semiDirectMethod",
                    nearestFaceAMI::typeName,
                    HashTable<word>(0),     // patchMap,
                    wordList(0),            // cuttingPatches
                    meshToMesh::procMapMethod::pmAABB,
                    false                   // do not normalise
                )
            );
        }
    }
}

bool Foam::functionObjects::LighthillIntegral::buildDummyCase() const
{
    bool ok = true;

    if (postProcess && Pstream::master())
    {
        // save the old sources
        if (fileHandler().isDir(aaRootPath()/vsDir) && !fileHandler().isDir(aaRootPath()/"volumeSource_old"))
        {
            bool ok = fileHandler().mv(aaRootPath()/vsDir, aaRootPath()/"volumeSource_old");
            if (ok)
            {
                Info << "Volume source file exists, and is moved to volumeSource_old." << endl;
            }
            else
            {
                // stop to avoid data losing
                FatalErrorInFunction << "Volume source file exists but can not be backed up." << abort(FatalError);
            }
        }
    }

    if (Pstream::master())
    {
        // copy caseConstant
        fileName caseConstant(time_.globalPath() / time_.constant());
        fileName vsCaseConstant(volSrcPath_ / time_.caseConstant());
        fileHandler().mkDir(vsCaseConstant);
        if (hasAcousticMesh())
        {
            ok = fileHandler().cp(caseConstant/acousticMeshName()/"polyMesh", vsCaseConstant/"polyMesh");
        }
        else
        {
            ok = fileHandler().cp(caseConstant/"polyMesh", vsCaseConstant/"polyMesh");
        }

        // copy for system
        fileName vsCaseSystem(volSrcPath_ / time_.caseSystem());
        ok = fileHandler().cp(time_.globalPath()/time_.system(), vsCaseSystem) && ok;

        if (debug)
        {
            Info<< indent << "Copied case constant directory (" << ok << ")" << endl;
        }
    }

    // copy for processor constant
    if(Pstream::parRun())
    {
        // use scatter to wait for master to finish
        label dummyAction = 1;
        Pstream::scatter(dummyAction);

        fileName procConstant(time_.path() / time_.constant());
        fileName vsProcConstant(volSrcPath_ / time_.constant() );
        if (hasAcousticMesh())
        {
            ok = fileHandler().cp(procConstant/acousticMeshName()/"polyMesh", vsProcConstant/"polyMesh") && ok;
        }
        else
        {
            ok = fileHandler().cp(procConstant/"polyMesh", vsProcConstant/"polyMesh") && ok;
        }
        if (debug)
        {
            Info<< indent << "Copied processor constant directory (" << ok << ")" << endl;
        }
    }

    if (debug)
    {
        if (ok)
        {
            Info << "    Created dummy volume source case successfully." << endl;
        }
        else
        {
            Info << "    Failed to create dummy volume source case." << endl;
        }
    }

    return ok;
}


Foam::tmp<Foam::scalarField>
Foam::functionObjects::LighthillIntegral::currentSoundPressure
(
    const point& obsPos,
    scalarField& invR,
    bool debugInfo
)
{
    chooseComponent(Tuu_,     Ttao_,     D_,     T_,   debugInfo);
    chooseComponent(dTuudt_,  dTtaodt_,  dDdt_,  dTdt_ );
    chooseComponent(dTuuddt_, dTtaoddt_, dDddt_, dTddt_);

    if (debug>10 && debugInfo)
    {
        const scalarField& V = acousticMesh().V().field();
        Info<< indent << "Source sum at t=" << sourceTime_ << nl
            << indent << "D:   " << gSum(D_ * V)
            << "  " << gSum(dDdt_ * V) << "  " << gSum(dDddt_ * V) << endl;
        Info<< indent << "Tuu: " << gSum(Tuu_ * V) << nl
            << indent << "   0:" << gSum(Tuu0_ * V) << nl
            << indent << "  dt:" << gSum(dTuudt_ * V) << nl
            << indent << " ddt:" << gSum(dTuuddt_ * V) << endl;
        Info<< indent << "Ttao:" << gSum(Ttao_ * V) << nl
            << indent << "   0:" << gSum(Ttao0_ * V) << nl
            << indent << "  dt:" << gSum(dTtaodt_ * V) << nl
            << indent << " ddt:" << gSum(dTtaoddt_ * V) << endl;
        Info<< indent << "T:   " << gSum(T_ * V) << nl
            << indent << "   0:" << gSum(T0_ * V) << nl
            << indent << "  dt:" << gSum(dTdt_ * V) << nl
            << indent << " ddt:" << gSum(dTddt_ * V) << endl;
    }

    // calculation
    const fvMesh& aMesh = acousticMesh();

    return formulaLighthill
    (
        obsPos,
        T_,
        dTdt_,
        dTddt_,
        Center_,
        invR
    ) * aMesh.V().field();
}


Foam::tmp<Foam::scalarField>
Foam::functionObjects::LighthillIntegral::oldSoundPressure
(
    const point& obsPos,
    scalarField& invR,
    bool debugInfo
)
{
    chooseComponent(Tuu0_,     Ttao0_,     D0_,     T0_    );
    chooseComponent(dTuudt0_,  dTtaodt0_,  dDdt0_,  dTdt0_ );
    chooseComponent(dTuuddt0_, dTtaoddt0_, dDddt0_, dTddt0_);

    if (debug>10 && debugInfo)
    {
        const scalarField& V = acousticMesh().V().field();
        Info<< indent << "Source sum at t=" << sourceTime_ << nl
            << indent << "D0:   " << gSum(D0_ * V)
            << "  " << gSum(dDdt0_ * V) << "  " << gSum(dDddt0_ * V) << endl;
        Info<< indent << "Tuu0: " << gSum(Tuu0_ * V) << nl
            << indent << "  dt0:" << gSum(dTuudt0_ * V) << nl
            << indent << " ddt0:" << gSum(dTuuddt0_ * V) << endl;
        Info<< indent << "Ttao0:" << gSum(Ttao0_ * V) << nl
            << indent << "  dt0:" << gSum(dTtaodt0_ * V) << nl
            << indent << " ddt0:" << gSum(dTtaoddt0_ * V) << endl;
        Info<< indent << "T0:   " << gSum(T0_ * V) << nl
            << indent << "  dt0:" << gSum(dTdt0_ * V) << nl
            << indent << " ddt0:" << gSum(dTddt0_ * V) << endl;
    }

    const fvMesh& aMesh = acousticMesh();
    return formulaLighthill
        (
            obsPos,
            T0_,
            dTdt0_,
            dTddt0_,
            Center0_,
            invR
        ) * aMesh.V().field();
}


void Foam::functionObjects::LighthillIntegral::chooseComponent
(
    const symmTensorField& Tuu,
    const symmTensorField& Ttao,
    const scalarField& D,
    symmTensorField& T,
    bool debugInfo
)
{
    if (debug <= 10) return;
    const symmTensor identityTensor(sphericalTensor::I);

    label choice = debug - 10;
    if(choice & 1)
    {
        T = Tuu - Ttao + D * identityTensor;
        if (debugInfo)
        {
            Info << indent << "Total Lighthill T." << endl;
        }
    }
    else if (choice & 2)
    {
        T = Tuu;
        if (debugInfo)
        {
            Info << indent << "uu component of Lighthill T." << endl;
        }
    }
    else if (choice & 4)
    {
        T = -Ttao;
        if (debugInfo)
        {
            Info << indent << "tao component of Total Lighthill T." << endl;
        }
    }
    else if (choice & 8)
    {
        T = D * identityTensor;
        if (debugInfo)
        {
            Info << indent << "density component of Total Lighthill T." << endl;
        }
    }
}


Foam::tmp<Foam::scalarField>
Foam::functionObjects::LighthillIntegral::formulaLighthill
(
    const point& obsPos,
    const symmTensorField& T,
    const symmTensorField& dTdt,
    const symmTensorField& dTddt,
    const pointField& C,
    scalarField& invR
) const
{
    const scalar invC0 = 1.0 / c0();
    const scalar invC0Sqr = invC0 * invC0;
    const symmTensor identityTensor(sphericalTensor::I); // (1., 0., 0., 1., 0., 1.);
    const scalar pi4Factor = 0.25 / pi;

    scalarField pDashV1, pDashV2;
    vectorField rVector;

    rVector = obsPos - C;
    if (U0NonZero())
    {
        movingRVector(rVector);
    }
    invR = 1.0 / (Foam::mag(rVector) + ROOTVSMALL);
    rVector *= invR;

    symmTensorField rTensor(rVector.size());
    vectorToSymmTensor(rVector, rTensor);

    pDashV1 = (rTensor && dTddt) * (invR * invC0Sqr);
    pDashV2 = ((3.0 * rTensor - identityTensor) && (dTdt + T * invR)) * (invR * invR * invC0);
    tmp<Foam::scalarField> pDashPtr;

    if (debug & 16)
    {
        pDashPtr = pDashV1 * pi4Factor;
    }
    else if (debug & 32)
    {
        pDashPtr = ((3.0 * rTensor - identityTensor) && dTdt) * (invR * invR * invC0) * pi4Factor;
    }
    else if (debug & 64)
    {
        pDashPtr = ((3.0 * rTensor - identityTensor) && T) * (invR * invR * invR * invC0) * pi4Factor;
    }
    else
    {
        pDashPtr = (pDashV1 + pDashV2) * pi4Factor;
    }
    return pDashPtr;
}


void Foam::functionObjects::LighthillIntegral::storeOldSources()
{
    // need to check whether the current one has content
    // if not, use zero for old sources
    sourceTime0_ = sourceTime_;
    const fvMesh& aMesh = acousticMesh();
    bool noOldValue = T_.size() < aMesh.nCells();
    if (noOldValue)
    {
        T0_.resize(aMesh.nCells(), symmTensor::zero);
        dTdt0_.resize(aMesh.nCells(), symmTensor::zero);
        dTddt0_.resize(aMesh.nCells(), symmTensor::zero);

        D0_.resize(aMesh.nCells(), 0.);
        dDdt0_.resize(aMesh.nCells(), 0.);
        dDddt0_.resize(aMesh.nCells(), 0.);

        Center0_ = aMesh.C().primitiveField();

        if (debug)
        {
            Tuu0_.resize(aMesh.nCells(), symmTensor::zero);
            dTuudt0_.resize(aMesh.nCells(), symmTensor::zero);
            dTuuddt0_.resize(aMesh.nCells(), symmTensor::zero);

            Ttao0_.resize(aMesh.nCells(), symmTensor::zero);
            dTtaodt0_.resize(aMesh.nCells(), symmTensor::zero);
            dTtaoddt0_.resize(aMesh.nCells(), symmTensor::zero);
        }

        nOldValues_ = 0;
    }
    else
    {
        T0_     = T_;
        dTdt0_  = dTdt_;
        dTddt0_ = dTddt_;

        D0_     = D_;
        dDdt0_  = dDdt_;
        dDddt0_ = dDddt_;

        Center0_ = Center_;

        if (debug)
        {
            Tuu0_     = Tuu_;
            dTuudt0_  = dTuudt_;
            dTuuddt0_ = dTuuddt_;

            Ttao0_     = Ttao_;
            dTtaodt0_  = dTtaodt_;
            dTtaoddt0_ = dTtaoddt_;
        }

        if (nOldValues_ < 10)
        {
            nOldValues_++;
        }
    }
}


void Foam::functionObjects::LighthillIntegral::sourceDdt()
{
    if (sourceTime_ - sourceTime0_ > VSMALL && nOldValues_ > 0)
    {
        if (debug)
        {
            Info << indent << "Calculte DDT for " << sourceTime_ << ", " << sourceTime0_ << endl;
        }

        // necessary if considering moving mesh in acousticDdt
        // correctMeshPhi();

        const scalar rDeltaT = 1.0 / (sourceTime_ - sourceTime0_);

        acousticDdt(D0_,    D_,    dDdt_,    rDeltaT);
        if (debug)
        {
            acousticDdt(Tuu0_,  Tuu_,  dTuudt_,  rDeltaT);
            acousticDdt(Ttao0_, Ttao_, dTtaodt_, rDeltaT);
        }
        else
        {
            acousticDdt(T0_,  T_,  dTdt_,  rDeltaT);
        }

        if (nOldValues_ > 1)
        {
            acousticDdt(dDdt0_,    dDdt_,    dDddt_,    rDeltaT);
            if (debug)
            {
                acousticDdt(dTuudt0_,  dTuudt_,  dTuuddt_,  rDeltaT);
                acousticDdt(dTtaodt0_, dTtaodt_, dTtaoddt_, rDeltaT);
            }
            else
            {
                acousticDdt(dTdt0_,  dTdt_,  dTddt_,  rDeltaT);
            }
        }
        else
        {
            const label nCells = acousticMesh().nCells();
            dDddt_  = scalarField(nCells, 0.);
            if (debug)
            {
                dTuuddt_  = symmTensorField(nCells, symmTensor::zero);
                dTtaoddt_ = symmTensorField(nCells, symmTensor::zero);
            }
            else
            {
                dTddt_ = symmTensorField(nCells, symmTensor::zero);
            }
        }

        if (debug)
        {
            const symmTensor identityTensor(sphericalTensor::I);
            dTdt_  = dTuudt_  - dTtaodt_  + dDdt_  * identityTensor;
            dTddt_ = dTuuddt_ - dTtaoddt_ + dDddt_ * identityTensor;
        }
    }
    else
    {
        if (debug)
        {
            Info << indent << "Make DDT to zero." << endl;
        }
        const label nCells = acousticMesh().nCells();
        dDdt_   = scalarField(nCells, 0.);
        dDddt_  = scalarField(nCells, 0.);
        if (debug)
        {
            dTuudt_   = symmTensorField(nCells, symmTensor::zero);
            dTuuddt_  = symmTensorField(nCells, symmTensor::zero);
            dTtaodt_  = symmTensorField(nCells, symmTensor::zero);
            dTtaoddt_ = symmTensorField(nCells, symmTensor::zero);
        }
        dTdt_     = symmTensorField(nCells, symmTensor::zero);
        dTddt_    = symmTensorField(nCells, symmTensor::zero);
    }
}

// * * * * * * * * * * * * Protected Member Functions * * * * * * * * * * * //

Foam::instantList Foam::functionObjects::LighthillIntegral::volumeSourceTimes() const
{
    // check if volumeSource result exist
    if (!fileHandler().isDir(volSrcPath_))
    {
        return instantList(0);
    }

    return fileHandler().findTimes(volSrcPath_, "DonotConsiderConstant");
}

//-
void Foam::functionObjects::LighthillIntegral::calcVolSoundPressure()
{
    scalarField invR, invROld;
    scalarField pDash, oldpDash;
    const label& nCells = acousticMesh().nCells();
    // time_.deltaTValue() is the deltaT between current Value and oldTime()
    // time_.deltaT0Value() is between oldTime() and oldOldTime()

    forAll(observers().observerPositions(), obsi)
    {
        const point& obsPos = observers().observerPositions()[obsi];

        pDash = currentSoundPressure(obsPos, invR, obsi == 0);
        oldpDash = oldSoundPressure(obsPos, invROld);

        for (label srci = 0; srci < nCells; ++srci)
        {
            const scalar obsTime = sourceTime_ + 1.0 / (invR[srci] * c0());
            const scalar oldTime = sourceTime0_ + 1.0 / (invROld[srci] * c0());

            observers().addSoundPressure
            (
                obsi,
                oldTime,
                obsTime,
                oldpDash[srci],
                pDash[srci]
            );
        }
    }
}

Foam::tmp<Foam::volSymmTensorField>
Foam::functionObjects::LighthillIntegral::devRhoReff() const
{
    typedef compressible::turbulenceModel cmpTurbModel;
    typedef incompressible::turbulenceModel icoTurbModel;

    if (foundObject<cmpTurbModel>(cmpTurbModel::propertiesName))
    {
        const cmpTurbModel& turb =
            lookupObject<cmpTurbModel>(cmpTurbModel::propertiesName);

        return turb.devRhoReff();
    }
    else if (foundObject<icoTurbModel>(icoTurbModel::propertiesName))
    {
        const incompressible::turbulenceModel& turb =
            lookupObject<icoTurbModel>(icoTurbModel::propertiesName);

        return rhoField()*turb.devReff();
    }
    else if (foundObject<fluidThermo>(fluidThermo::dictName))
    {
        const fluidThermo& thermo =
            lookupObject<fluidThermo>(fluidThermo::dictName);

        const volVectorField& U = lookupObject<volVectorField>("U");

        return -thermo.mu()*dev(twoSymm(fvc::grad(U)));
    }
    else if (foundObject<transportModel>("transportProperties"))
    {
        const transportModel& laminarT =
            lookupObject<transportModel>("transportProperties");

        const volVectorField& U = lookupObject<volVectorField>("U");

        return -rhoField()*laminarT.nu()*dev(twoSymm(fvc::grad(U)));
    }
    else if (foundObject<dictionary>("transportProperties"))
    {
        const dictionary& transportProperties =
            lookupObject<dictionary>("transportProperties");

        dimensionedScalar nu("nu", dimViscosity, transportProperties);

        const volVectorField& U = lookupObject<volVectorField>("U");

        return -rhoField()*nu*dev(twoSymm(fvc::grad(U)));
    }
    else
    {
        FatalErrorInFunction
            << "No valid model for viscous stress calculation"
            << exit(FatalError);

        return volSymmTensorField::null();
    }
}


void Foam::functionObjects::LighthillIntegral::calcVolumeSource()
{
    checkInterpolationMap();

    storeOldSources();

    const fvMesh& aMesh = acousticMesh();
    const symmTensor identityTensor(sphericalTensor::I); // (1., 0., 0., 1., 0., 1.);
    sourceTime_ = time_.value();

    // Initilize the variables (necessary?)
    if (D_.size() < aMesh.nCells())
    {
        D_.resize(aMesh.nCells(), 0.);
        Tuu_.resize(aMesh.nCells(), Zero);
        Ttao_.resize(aMesh.nCells(), Zero);
        Center_.resize(aMesh.nCells(), Zero);
    }

    if (debug)
    {
        Info << indent << "calculating volume source" <<nl;
        Info << incrIndent;
    }

    // need to calculate V_, D_, Tuu_, Ttao_
    // dDdt_, dDddt_, dTuudt_, dTuuddt_, dTtaodt_, dTtaoddt_

    // 1. The ordinary variables V_, D_, Tuu_, Ttao_, T_
    scalarField VolRatio(aMesh.nCells(), 0.);
    interpolate(scalarField(mesh_.nCells(), 1.), VolRatio);
    interpolate(mesh_.C(), Center_);
    Center_ /= VolRatio + ROOTVSMALL;

    scalarField rho = rhoField();
    {
        scalarField rhoDash = (rhoRef() - rho);
        interpolate(rhoDash, D_);
        D_ *= c0() * c0();
    }
    interpolate(devRhoReff()(), Ttao_);

    const volVectorField& U = lookupObject<volVectorField>("U");
    symmTensorField TUU(U.size());

    if (U0NonZero())
    {
        vectorField realU = U.primitiveField() + U0();
        vectorToSymmTensor(realU, TUU);
    }
    else
    {
        vectorToSymmTensor(U, TUU);
    }
    TUU *= rho;
    interpolate(TUU, Tuu_);

    T_ = Tuu_ - Ttao_ + D_ * identityTensor;

    // 2. Time derivatives
    sourceDdt();

    if (debug)
    {
        Info << indent << "Completed volume source." << endl;
        Info << decrIndent << endl;
    }
}

void Foam::functionObjects::LighthillIntegral::writeVolumeSource()
{
    // 1. write time information
    IOdictionary timeDict
    (
        IOobject
        (
            "sourceTime",
            volSrcPath_/time_.timeName()/"uniform",
            time_,
            IOobject::NO_READ,
            IOobject::NO_WRITE,
            false
        )
    );
    timeDict.add("sourceTime", Time::timeName(sourceTime_, 3 - Foam::log10(SMALL)));
    timeDict.add("sourceTime0", Time::timeName(sourceTime0_, 3 - Foam::log10(SMALL)));
    bool ok = fileHandler().writeObject(timeDict, IOstreamOption(IOstreamOption::ASCII), true);
    if (!ok)
    {
        FatalErrorInFunction
            << "Writing Time Info for Surface Source Failed" << exit(FatalError);
    }

    // 2. write changing mesh
    //== 2.1 change caseName of time
    Time& time = const_cast<Time&>(time_);
    auto sep = time_.caseName().find('/');
    fileName oldCaseName(time_.caseName());
    if (sep != std::string::npos)
    {
        fileName topCaseName = time_.caseName().substr(0, sep);
        fileName procName = time_.caseName().substr(sep+1);
        time.caseName() = topCaseName/outputPrefix/name()/vsDir/procName;
    }
    else
    {
        time.caseName() = time_.caseName()/outputPrefix/name()/vsDir;
    }

    //== 2.2 write the updated mesh information
    const fvMesh& aMesh = acousticMesh();
    IOstreamOption writeOpt(time_.writeFormat(), time_.writeCompression());

    const wordList meshComponents({"points", "faces", "owner", "neighbour", "boundary", "pointZones", "faceZones", "cellZones"});
    for (objectRegistry::const_iterator iter = aMesh.objectRegistry::cbegin(); iter != aMesh.objectRegistry::cend(); ++iter)
    {
        if ((*iter)->writeOpt() != IOobject::NO_WRITE && meshComponents.found((*iter)->name()))
        {
            (*iter)->writeObject(writeOpt, true);
        }
    }

    //== 2.3 restore the caseName of time
    time.caseName() = oldCaseName;

    // 3. write the volume sound source terms
    //== 3.1 initilize and header
    OFstream os(volSrcPath_/time_.timeName()/"sourceLH");
    if (!os.good())
    {
        FatalIOErrorInFunction(os)
            << "Fail to open file during writing volume source." << exit(FatalIOError);
    }

    if (format() == "binary")
    {
        os.format("binary");
        os.compression("on");
    }

    IOobject tmpIO
    (
        "sourceLH",
        volSrcPath_/time_.timeName(),
        time_,
        IOobject::NO_READ,
        IOobject::NO_WRITE,
        false
    );
    tmpIO.writeHeader(os);

    //== 3.2 the source terms
    T_.writeEntry(     "T",      os);
    D_.writeEntry(     "D",      os);
    Center_.writeEntry("C",      os);

    if (debug > 10)
    {
        Tuu_.writeEntry(     "Tuu",      os);
        Ttao_.writeEntry(     "Ttao",      os);
    }

    //== 3.3 end divider
    IOobject::writeEndDivider(os);
}

void Foam::functionObjects::LighthillIntegral::loadVolumeSource(const instant& curTime)
{
    storeOldSources();

    // 1. load time information
    IOdictionary timeDict
    (
        IOobject
        (
            "sourceTime",
            volSrcPath_/curTime.name()/"uniform",
            time_,
            IOobject::MUST_READ,
            IOobject::NO_WRITE,
            false
        )
    );

    sourceTime_ = timeDict.get<scalar>("sourceTime");

    if (debug)
    {
        Info<< indent << "Load lighthill sourceTime_ = " << sourceTime_
            << ", lighthill sourceTime0_ = " << sourceTime0_ << endl;
    }

     // 2. read the mesh update
     //== 2.1 change caseName to the correct directory
    auto sep = time_.caseName().find('/');
    fileName oldCaseName(time_.caseName());
    Time& time = const_cast<Time&>(time_);
    if (sep != std::string::npos)
    {
        fileName topCaseName = time_.caseName().substr(0, sep);
        fileName procName = time_.caseName().substr(sep+1);
        time.caseName() = topCaseName/outputPrefix/name()/vsDir/procName;
    }
    else
    {
        time.caseName() = time_.caseName()/outputPrefix/name()/vsDir;
    }

    //== 2.2 read the mesh update
    fvMesh& aMesh = const_cast<fvMesh&>(acousticMesh());
    auto state = aMesh.readUpdate();

    if (debug)
    {
        const wordList stateNames({"UNCHANGED", "POINTS_MOVED", "TOPO_CHANGE", "TOPO_PATCH_CHANGE"});
        Info << indent << "Mesh update state:" << stateNames[state] << endl;
    }

    //== 2.3 restore the time caseName
    time.caseName() = oldCaseName;

    // 3. read fileds
    IFstream is(volSrcPath_/curTime.name()/"sourceLH");
    if (!is.good())
    {
        FatalIOErrorInFunction(is)
            << "Fail to open file during reading volume source." << exit(FatalIOError);
    }
    if (format() == "binary")
    {
        is.format("binary");
    }
    dictionary sourceDict(is);
    const label nCells = aMesh.nCells();

    T_  = symmTensorField("T", sourceDict, nCells);
    D_  = scalarField("D", sourceDict, nCells);
    Center_ = vectorField("C", sourceDict, nCells);

    if (debug > 10)
    {
        Tuu_      = symmTensorField("Tuu",      sourceDict, nCells);
        Ttao_      = symmTensorField("Ttao",      sourceDict, nCells);
    }

    sourceDdt();

    if (debug)
    {
        const scalarField& V = acousticMesh().V().field();
        Info << incrIndent << indent << "Loaded volume source." << endl;
        Info<< indent << "Volume source sum at t=" << sourceTime_ << nl
            << indent << "D " << gSum(D_ * V) << "  " << gSum(dDdt_ * V)
            << "  " << gSum(dDddt_ * V)<< nl
            << indent << "D0 " << gSum(D0_ * V) << "  " << gSum(dDdt0_ * V)
            << "  " << gSum(dDddt0_ * V)<< nl
            << indent << "T  " << gSum(T_ * V) << "  " << gSum(dTdt_ * V)<<endl;
        Info << decrIndent;

        // for points output
        // double x[] = {-0.065, -0.055, -0.045, -0.035, -0.025, -0.015, -0.005, -0.035, -0.025, -0.015, -0.005};
        // double y[] = {0.145, 0.145, 0.145, 0.145, 0.145, 0.145, 0.145, 0.145, 0.145, 0.145, 0.145};
        // double z[] = {0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.015, 0.015, 0.015, 0.015};
        // const vectorField& C = acousticMesh().C().field();
        // const scalar eps = 0.0001;
        // for (label srci = 0; srci < nCells; ++srci)
        // {
        //     for (label pnti = 0; pnti < 11; ++pnti)
        //     {
        //         if (
        //             fabs(C[srci].x() - x[pnti]) < 0.0001 &&
        //             fabs(C[srci].y() - y[pnti]) < 0.0001 &&
        //             fabs(C[srci].z() - z[pnti]) < 0.0001
        //         )
        //         {
        //             Pout << pnti << ":" << dDddt_[srci] << ", " << Center_[srci] << nl;
        //         }
        //     }
        // }
    }
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::functionObjects::LighthillIntegral::LighthillIntegral
(
    const word& name,
    const Time& runTime,
    const dictionary& dict
)
:
    acousticAnalogyIntegral(name, runTime, dict),
    sourceTime_(time_.value()),
    sourceTime0_(time_.value()),
    nOldValues_(0),
    T_(),
    dTdt_(),
    dTddt_(),
    T0_(),
    dTdt0_(),
    dTddt0_(),
    Center_(),
    Center0_(),
    D_(),
    dDdt_(),
    dDddt_(),
    D0_(),
    dDdt0_(),
    dDddt0_(),
    Tuu_(),
    dTuudt_(),
    dTuuddt_(),
    Tuu0_(),
    dTuudt0_(),
    dTuuddt0_(),
    Ttao_(),
    dTtaodt_(),
    dTtaoddt_(),
    Ttao0_(),
    dTtaodt0_(),
    dTtaoddt0_(),
    mmPtr_(),
    cfdCellMap_(0),
    volSrcPath_(aaRootPath()/vsDir)
{
    const auto sep = time_.caseName().find('/');
    if (sep != std::string::npos)
    {
        fileName procName = time_.caseName().substr(sep+1);
        volSrcPath_ /= procName;
    }

    if
	(
		volumeSource()
		&&
		(
    		(postProcess && calcSoundSourceInPost())
			||
			(!postProcess && calcSoundSource())
		)
	)
	{
		buildDummyCase();
	}

    if(!cfdZoneIds().empty())
    {
        bitSet cfdCells(mesh_.nCells());
        for (label zonei: cfdZoneIds())
        {
            cfdCells.set(mesh_.cellZones()[zonei]);
        }
        fvMeshSubset cfdMeshSubset(mesh_, cfdCells);

        labelList cellMap(cfdMeshSubset.cellMap());
        cfdCellMap_.transfer(cellMap);
    }
}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //
